# rafamds framework
Uma framework muito pequena baseada em Laravel.

-----

## Disclaimer

Esta framework é simplesmente uma tentativa de tornar mais leve o Laravel, não a framework ou conceitos em si mas as ferramentas usadas. Como o Eloquent para as bases de dados, o blade como template engine a ainda alguns outros pacotes do Illuminate(routes, middlewares, debug).</p>
Para alterar esta página basta aceder ao ficheiro


## Instalar

```
$ composer create-project rafamds/framework <project name>
```

## License

Copyright 2017 RAFAEL SANTOS

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
