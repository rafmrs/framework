<html>
	<head>
		<title>rafamds framework</title>
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<div class="container">
			<div class="guide">
				<div class="title">
					<h1 class="text">rafamds framework</h1>
					<span class="subtext">Uma framework muito pequena baseada em Laravel.</span>
				</div>
				<div class="content">
					<h2>Disclaimer</h2>
					<p>Esta framework é simplesmente uma tentativa de tornar mais leve o Laravel, não a framework ou conceitos em si mas as ferramentas usadas. Como
						o Eloquent para as bases de dados, o blade como template engine a ainda alguns outros pacotes do Illuminate(routes, middlewares, debug).</p>
					<p>Para alterar esta página basta aceder ao ficheiro</p>
					<pre>resources/views/layout.blade.php</pre>
					<p>Ou no Controller de exemplo que está localizado em</p>
					<pre>src/Controllers/Example.php</pre>

					<h2>License</h2>
					<p>Copyright 2017 RAFAEL SANTOS<br>
					   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:<br>
					   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.<br>
					   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
				</div>
			</div>
		</div>
	</body>
</html>