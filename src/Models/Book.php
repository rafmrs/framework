<?php

namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Book extends Eloquent
{
	public $timestamps = false;
	protected $table = 'books';
	protected $fillable = ['name', 'author', 'edition', 'create'];
}