<?php

namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Session extends Eloquent
{
	public $timestamps = false;
	protected $table = 'sessions';
	protected $fillable = ['session_id', 'data', 'ip', 'agent', 'csrf'];
}