<?php

use Illuminate\Routing\Router;

$router->group(['namespace' => 'Controllers', 'as' => 'example-'], function(Router $router){
	$router->get('/', ['as' => 'index', 'uses' => 'Example@index']);
});

$router->group(['namespace' => 'Controllers', 'middleware' => 'Middlewares\Authenticate'], function(Router $router){
	$router->get('/auth', ['as' => 'index', 'uses' => 'Example@index']);
});