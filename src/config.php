<?php

return [
    'debug' => 1,
    'timezone' => 'Europe/Lisbon',

    /**
     * Database settings
     */
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'name' => 'teste',
        'user' => 'root',
        'pass' => 'root',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => ''
    ]
];