<?php

namespace Core;

use Models\Session;

class SessionManager
{

	/**
	 *
	 * Start a new session or resume
	 *
	 */
	static public function start()
	{
		session_start();
		$id = session_id();
		if(!Session::where('session_id', $id)->first()){
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
			$ip = $_SERVER['REMOTE_ADDR'];
			if(empty($_SESSION['frameworkcsrf'])){
				if(function_exists('mcrypt_create_iv')){
					$_SESSION['frameworkcsrf'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
				}else{
					$_SESSION['frameworkcsrf'] = bin2hex(openssl_random_pseudo_bytes(32));
				}
			}
			$csrf = $_SESSION['frameworkcsrf'];
			Session::create([
				'session_id' => $id,
				'ip' => $ip,
				'agent' => $user_agent,
				'csrf' => $csrf
			]);
		}
	}

	/**
	 *
	 * Verify if csrf token is valid
	 *
	 * @param string $token Hash token to check csrf
	 * @return boolean
	 *
	 */
	static public function verify($token)
	{
		$id = session_id();
		$session = Session::where('session_id', $id)->first();
		if($session){
			if(hash_equals($session->csrf, $token)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 *
	 * Get csrf token
	 *
	 * @return string
	 *
	 */
	static public function csrf()
	{
		$id = session_id();
		$session = Session::where('session_id', $id)->first();
		if($session){
			return $session->csrf;
		}else{
			return '';
		}
	}

	/**
	 *
	 * Write a new session
	 *
	 * @param string $key Key name of the session
	 * @param object $value 
	 *
	 */
	static public function write($key, $value)
	{

	}

	/**
	 *
	 * Read an existing session
	 *
	 * @param string $key Key name of the session
	 *
	 */
	static public function read($key)
	{

	}

	/**
	 *
	 * Delete a session
	 *
	 * @param string $key Key name of the session
	 *
	 */
	static public function remove($key)
	{

	}
}