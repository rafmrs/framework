<?php

namespace Core;

use duncan3dc\Laravel\BladeInstance;

class Controller
{
	public function __construct()
	{
		$this->view = new BladeInstance(ROOT_PATH . "/resources/views", ROOT_PATH . "/resources/cache");
		$this->view->directive('raw', function($expression) {
			return "<?php $expression; ?>";
		});
	}
}