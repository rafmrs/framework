<?php

namespace Middlewares;

use App;
use Closure;

class Authenticate
{
	/**
	 * Run the request filter.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// Example middleware
		if(400 <= 200) {
			return $next($request);
		}else{
			return App::redirect()->to('/');
		}
	}

}