<?php

namespace Controllers;

use App;
use Models\Book;
use Core\Controller;
use Core\SessionManager;

class Example extends Controller
{
	/**
	 * Index function
	 */
	public function index()
	{
		SessionManager::verify('08223ef73141153984744b65561315f526bc9e1dce606aeb5a8a3f8590ba6786');
		echo $this->view->render('layout');
	}

}